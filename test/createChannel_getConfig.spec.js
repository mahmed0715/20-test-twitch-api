process.env.NODE_ENV = 'test';

let chai = require('chai');
var supertest = require("supertest");
let chaiHttp = require('chai-http');
let should = chai.should();
const API_BASE = "https://wv0ug07716.execute-api.us-west-1.amazonaws.com/prod";
// const API_BASE = "https://hwjpl9p9mj.execute-api.us-west-1.amazonaws.com/dev";
const server = supertest.agent(API_BASE);

chai.use(chaiHttp);

describe('Create Channel for user', () => {
  let API = '/channel/create';
  describe('/POST channel/create', () => {
      it
      // .skip
      ('it should create a channle and bind to that user', (done) => {
       server
        .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjQxMDg5NjE5ZjI2MmEwNzI3ZTJlIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3MzM5NTA3NiwiZXhwaXJlcyI6MTU4MzEyNTM5NTA3NiwiaWF0IjoxNTY3NTczMzk1fQ.HE5-5CcwZP1jkjAL3_53jGVm0NwACxY5ourhVWYAPUg')    
       .send({"channelId": 12121122})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      // .skip
      ('it should create channle no 3 and bind to that user', (done) => {
       server
        .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjUxM2EzMjA0MTcwNjU5MTJhYzYwIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3NjQyODA4NiwiZXhwaXJlcyI6MTU4MzEyODQyODA4NiwiaWF0IjoxNTY3NTc2NDI4fQ.qtlpq-q-67oG4tvbcC4zsv2UD7kXAwJDkKVw3Xy3yNU')    
       .send({"channelId": 12121123})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      // .skip
      ('it should create channle no 4 and bind to that user', (done) => {
       server
        .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjUxYTczMjA0MTcwNjU5MTJhYzYxIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3NjUwNDQwNCwiZXhwaXJlcyI6MTU4MzEyODUwNDQwNCwiaWF0IjoxNTY3NTc2NTA0fQ.60v8f_KJQdEwEB-pGiY2_bGsT9OX96M5hulZGzsimDo')    
       .send({"channelId": 12121124})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      // .skip
      ('it should create channle no 5 and bind to that user', (done) => {
       server
        .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjUxZmYzMjA0MTcwNjU5MTJhYzYyIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3NjYyOTE5MSwiZXhwaXJlcyI6MTU4MzEyODYyOTE5MSwiaWF0IjoxNTY3NTc2NjI5fQ.7cN5cD3jplVyn0BqMCsGFESJwzNLwahBWJiMnK3xWo8')    
       .send({"channelId": 12121125})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      // .skip
      ('it should create channle no 6 and bind to that user', (done) => {
       server
        .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjUyNmEzMjA0MTcwNjU5MTJhYzYzIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3NjcxOTYxNywiZXhwaXJlcyI6MTU4MzEyODcxOTYxNywiaWF0IjoxNTY3NTc2NzE5fQ.yKXzPxaukHpQyqFZPEsY7JoujnF8b-utg7ovFsyxeNc')    
       .send({"channelId": 12121126})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it('it should not create channle because of missing auth token', (done) => {
        server
         .post(API)
         //.set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjQxMDg5NjE5ZjI2MmEwNzI3ZTJlIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3MzM5NTA3NiwiZXhwaXJlcyI6MTU4MzEyNTM5NTA3NiwiaWF0IjoxNTY3NTczMzk1fQ.HE5-5CcwZP1jkjAL3_53jGVm0NwACxY5ourhVWYAPUg')    
        .send({"channelId": 212121122})
        .expect("Content-type",/json/)
        .expect(401)
             .end((err, res) => {
                // console.log(err, res)
                 if (err) done(err);
                   res.should.have.status(401);
                   res.body.should.have.property('message').eql('Unauthorized');
               done();
             });
       });
       it('it should not create channle because of wrong auth token should get 401', (done) => {
        server
         .post(API)
         .set('Authorization','eyJhbGciWQ2ZjQxMDg5NjE5ZjI2MmEwNzI3ZTJlIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3MzM5NTA3NiwiZXhwaXJlcyI6MTU4MzEyNTM5NTA3NiwiaWF0IjoxNTY3NTczMzk1fQ.HE5-5CcwZP1jkjAL3_53jGVm0NwACxY5ourhVWYAPUg')    
        .send({"channelId": 212121122})
        .expect("Content-type",/json/)
        .expect(401)
             .end((err, res) => {
                // console.log(err, res)
                 if (err) done(err);
                   res.should.have.status(401);
                   res.body.should.have.property('message').eql('Unauthorized');
               done();
             });
       });
       it('it should not create channle because of no channel id but auth token good should get 400 bad request', (done) => {
        server
         .post(API)
        .set('Authorization','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVVUlEIjoiNWQ2ZjQxMDg5NjE5ZjI2MmEwNzI3ZTJlIiwiZGV2ZWxvcGVyIjpmYWxzZSwiY3JlYXRlZCI6MTU2NzU3MzM5NTA3NiwiZXhwaXJlcyI6MTU4MzEyNTM5NTA3NiwiaWF0IjoxNTY3NTczMzk1fQ.HE5-5CcwZP1jkjAL3_53jGVm0NwACxY5ourhVWYAPUg')    
        .expect("Content-type",/json/)
        .expect(400)
             .end((err, res) => {
                // console.log(err, res)
                 if (err) done(err);
                 res.should.have.status(400);
                   res.body.should.have.property('success').eql(false);
               done();
             });
       });
  });
});


describe('Get Channel config for channleId', () => {
  let API = '/channel/getConfig';
  describe('/POST channel/getConfig', () => {
      it

      ('it should get a channle config: success true', (done) => {
       server
       .post(API)
       .send({"channelId": 12121122})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      
      ('it should get config channel no 3', (done) => {
       server
       .post(API)
       .send({"channelId": 12121123})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      
      ('it should get config channle no 4 ', (done) => {
       server
       .post(API)
       .send({"channelId": 12121124})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      
      ('it should get config channle no 5 ', (done) => {
       server
       .post(API)
       .send({"channelId": 12121125})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it
      // 
      ('it should get config channle no 6', (done) => {
       server
       .post(API)
       .send({"channelId": 12121126})
       .expect("Content-type",/json/)
       .expect(200)
            .end((err, res) => {
                // console.log(err, res)
                if (err) done(err);
                  res.should.have.status(200);
                  res.body.should.have.property('success').eql(true);
              done();
            });
      });
      it('it should not get config of channel for non existing channel should get 400 bad request', (done) => {
        server
        .post(API)
        .send({"channelId": 212121231})
        .expect("Content-type",/json/)
        .expect(400)
             .end((err, res) => {
                // console.log(err, res)
                 if (err) done(err);
                   res.should.have.status(400);
                   res.body.should.have.property('error');
               done();
             });
       });
       it('it should not get config of channel if no channel id provided should get 400 bad request', (done) => {
        server
        .post(API)
        .expect("Content-type",/json/)
        .expect(400)
             .end((err, res) => {
                // console.log(err, res)
                 if (err) done(err);
                 res.should.have.status(400);
                   res.body.should.have.property('success').eql(false);
               done();
             });
       });
  });
});
